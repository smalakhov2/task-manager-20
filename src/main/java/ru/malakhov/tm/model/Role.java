package ru.malakhov.tm.model;

import org.jetbrains.annotations.Nullable;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    private final String displayName;

    Role(final @Nullable String displayName) {
        this.displayName = displayName;
    }

    public @Nullable String getDisplayName() {
        return displayName;
    }

}
