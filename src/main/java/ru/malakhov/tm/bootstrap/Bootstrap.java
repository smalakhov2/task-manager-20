package ru.malakhov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.service.*;
import ru.malakhov.tm.util.TerminalUtil;
import ru.malakhov.tm.exception.system.UnknownArgumentException;
import ru.malakhov.tm.exception.system.UnknownCommandException;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.model.Role;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final @NotNull IUserRepository userRepository = new UserRepository();

    private final @NotNull IUserService userService = new UserService(userRepository);

    private final @NotNull ICommandRepository commandRepository = new CommandRepository();

    private final @NotNull ITaskRepository taskRepository = new TaskRepository();

    private final @NotNull ITaskService taskService = new TaskService(taskRepository);

    private final @NotNull IProjectRepository projectRepository = new ProjectRepository();


    private final @NotNull IProjectService projectService = new ProjectService(projectRepository);

    private final @NotNull IAuthService authService = new AuthService(userService);

    private final @NotNull ICommandService commandService = new CommandService(commandRepository);

    private final @NotNull IDomainService domainService = new DomainService(taskService, projectService, userService);


    private final @NotNull Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final @NotNull Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        putCommands(commandService.getCommandList());
    }

    private void putCommands(@Nullable List<AbstractCommand> commandList) {
        for (@Nullable AbstractCommand command: commandList) {
            if (command == null) return;
            command.setServiceLocator(this);
            commands.put(command.name(), command);
            arguments.put(command.argument(), command);
        }
    }

    private void createDefaultUsers(){
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin","admin", Role.ADMIN);
    }

    private static void printHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final @Nullable String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        try{
            parseArg(arg);
        } catch (Exception e){
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    private void parseArg(final @Nullable String arg) throws Exception{
        if (arg == null || arg.isEmpty()) return;
        final @Nullable AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        authService.checkRole(argument.role());
        argument.execute();
    }

    private void parseCommand(final String cmd) throws Exception{
        if (cmd == null || cmd.isEmpty()) return;
        final @Nullable AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authService.checkRole(command.role());
        command.execute();
    }

    public void run(final String[] args) {
        printHello();
        if (parseArgs(args)) System.exit(0);
        createDefaultUsers();
        process();
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return authService;
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

}