package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

}