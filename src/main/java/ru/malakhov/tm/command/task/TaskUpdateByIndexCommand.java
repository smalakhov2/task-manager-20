package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public @NotNull String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final @Nullable Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().findOneByIndex(userId, index);
        System.out.println("ENTER NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final @Nullable String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        System.out.println("[OK]");
    }

}