package ru.malakhov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import static ru.malakhov.tm.constant.DataConst.BINARY_PATH;

public class DataBase64LoadCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "db64-load";
    }

    @Override
    public @NotNull String description() {
        return "Load base64 date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64 = Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(BINARY_PATH)));
        final byte[] decBase64 = new BASE64Decoder().decodeBuffer(base64);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decBase64);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        final Domain domain = (Domain) objectInputStream.readObject();

        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);

        byteArrayInputStream.close();
        objectInputStream.close();
        System.out.println("[OK]");

    }

}