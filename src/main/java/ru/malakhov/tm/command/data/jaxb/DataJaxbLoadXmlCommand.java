package ru.malakhov.tm.command.data.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import java.io.File;

public class DataJaxbLoadXmlCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-jaxb-xml-load";
    }

    @Override
    public @NotNull String description() {
        return "Load xml jaxb date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML LOAD]");
        final @NotNull File file = new File(DataConst.DATA_JAXB_XML_PATH);

        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Domain domain = (Domain) jaxbContext.createUnmarshaller().unmarshal(file);

        final @NotNull IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);

        System.out.println("[OK]");
    }

}