package ru.malakhov.tm.command.data.jaxb;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxbLoadJsonCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-jaxb-json-load";
    }

    @Override
    public @NotNull String description() {
        return "Load json jaxb date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON LOAD]");
        final @NotNull File file = new File(DataConst.DATA_JAXB_JSON_PATH);

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        final @NotNull Domain domain = (Domain) unmarshaller.unmarshal(file);

        final @NotNull IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);

        System.out.println("[OK]");
    }

}