package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataFasterxmlSaveXmlCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-xml-save";
    }

    @Override
    public @NotNull String description() {
        return "Save xml fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML XML SAVE]");
        final @NotNull Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final @NotNull File file = new File(DataConst.DATA_FASTERXML_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final @NotNull ObjectMapper objectMapper = new XmlMapper();
        final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.DATA_FASTERXML_XML_PATH);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}