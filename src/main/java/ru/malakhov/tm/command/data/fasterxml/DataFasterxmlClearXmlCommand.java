package ru.malakhov.tm.command.data.fasterxml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataFasterxmlClearXmlCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-xml-clear";
    }

    @Override
    public @NotNull String description() {
        return "Clear xml fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML XML CLEAR]");
        final @NotNull File file = new File(DataConst.DATA_FASTERXML_XML_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}
