package ru.malakhov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.DataConst.BINARY_PATH;

public class DataBinaryClearCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "binary-clear";
    }

    @Override
    public @NotNull String description() {
        return "Clear binary data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY CLEAR]");
        final @NotNull File file = new File(BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}