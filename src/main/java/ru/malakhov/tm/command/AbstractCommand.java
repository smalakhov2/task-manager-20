package ru.malakhov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.model.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @Nullable
    public abstract String argument();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    @Nullable
    public Role[] role() {
        return null;
    }


}