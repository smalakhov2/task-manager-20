package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeLoginCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.CHANGE_LOGIN;
    }

    @Override
    public @NotNull String description() {
        return "Change login.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().changeLogin(userId, login);
        System.out.println("[OK]");
    }

}