package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeFirstNameCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.CHANGE_FIRST_NAME;
    }

    @Override
    public @NotNull String description() {
        return "Change first name.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE FIRST NAME]");
        System.out.println("ENTER NEW FIRST NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        serviceLocator.getUserService().changeFirstName(userId, name);
        System.out.println("[OK]");
    }

}