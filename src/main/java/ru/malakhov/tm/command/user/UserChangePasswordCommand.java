package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.TerminalUtil;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;

import java.util.Objects;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.CHANGE_PASSWORD;
    }

    @Override
    public @NotNull String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        final @Nullable  String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        final @Nullable String currentPassword = TerminalUtil.nextLine();
        if (Objects.equals(HashUtil.salt(currentPassword), serviceLocator.getUserService().findById(userId).getPasswordHash())) {
            System.out.println("ENTER NEW PASSWORD:");
            final @Nullable String newPassword = TerminalUtil.nextLine();
            serviceLocator.getUserService().changePassword(userId, newPassword);
            System.out.println("[OK]");
        }
        throw new EmptyPasswordException();
    }

}