package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;

public final class UserProfileCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.PROFILE;
    }

    @Override
    public @NotNull String description() {
        return "Profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROFILE]");
        String[] profile = serviceLocator.getUserService().profile(userId);
        for (@Nullable String s : profile) {
            System.out.println(s);
        }
    }

}