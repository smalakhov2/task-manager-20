package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}